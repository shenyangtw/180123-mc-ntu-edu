//Slideshow tab focus
// Set <a href="https://www.google.com.tw/" onfocus="focusShow('#slideshow .uk-dotnav a', event)" onkeydown="enterOpenUrl('_blank', event)">Banner1</a> on <ul class="uk-dotnav">
function slideShowFocus(slideshow, tabsArray, thisFocus) {
  var slideshow = document.querySelector(slideshow)
  var tabs = document.querySelectorAll(tabsArray)
  for (var i = 0;i < tabs.length;i++) {
    // tabs[i] = UIkit.slideshow(slideshow).show(i)
    if (thisFocus == tabs[i]) {
      UIkit.slideshow(slideshow).show(i)
    }
  }
}
function enterOpenUrl(thisKeyDown, event) {
  if (event.keyCode === 13) {
    let target = thisKeyDown.getAttribute('target')
    window.open(thisKeyDown.getAttribute('href'), target ? target : "_self")
  }
}

$(function () {
  // For AA of javascript detecting
  // $('[lang="zh_TW"] body').prepend('<noscript>您的瀏覽器不支援JavaScript功能，若網頁功能無法正常使用時，請開啟瀏覽器JavaScript狀態！</noscript>')
  // $('[lang="en_US"] body').prepend("<noscript>Your browser's JavaScript is disabled, please turn on!</noscript>")
	$('[lang="zh_TW"] .uk-slidenav-next').html('<span class="sr-only">下一張圖</span>');
	$('[lang="en_US"] .uk-slidenav-previous').html('<span class="sr-only">previous image</span>');
	$('[lang="en_US"] .uk-slidenav-next').html('<span class="sr-only">next image</span>');
	$('[lang="zh_TW"] .uk-slidenav-previous').attr("title", "上一張圖");
	$('[lang="zh_TW"] .uk-slidenav-next').attr("title", "下一張圖");
	$('[lang="en_US"] .uk-slidenav-previous').attr("title", "previous image");
	$('[lang="en_US"] .uk-slidenav-next').attr("title", "next image");
	$('[href="Honor.action"]').hide();
  // For AA of uk-lightbox
  $('body').on("showitem.uk.lightbox",'.photo_list>a' ,function () {
	  setTimeout(
        function () {
		  console.log("Element is  visible.");
		  $('[lang="zh_TW"] .uk-modal a.uk-modal-close').html('<span class="sr-only">關閉</span>');
		  $('[lang="zh_TW"] .uk-lightbox-content a.uk-slidenav-previous').html('<span class="sr-only">上一張圖</span>');
		  $('[lang="zh_TW"] .uk-lightbox-content a.uk-slidenav-next').html('<span class="sr-only">下一張圖</span>');
		  $('[lang="en_US"] .uk-modal a.uk-modal-close').html('<span class="sr-only">close</span>');
		  $('[lang="en_US"] .uk-lightbox-content a.uk-slidenav-previous').html('<span class="sr-only">previous image</span>');
		  $('[lang="en_US"] .uk-lightbox-content a.uk-slidenav-next').html('<span class="sr-only">next image</span>');
		  $('[lang="zh_TW"] .uk-modal a.uk-modal-close').attr("title", "關閉");
		  $('[lang="en_US"] .uk-modal a.uk-modal-close').attr("title", "close");
		  $('[lang="zh_TW"] .uk-lightbox-content a.uk-slidenav-previous').attr("title", "上一張圖");
		  $('[lang="en_US"] .uk-lightbox-content a.uk-slidenav-previous').attr("title", "previous image");
		  $('[lang="zh_TW"] .uk-lightbox-content a.uk-slidenav-next').attr("title", "下一張圖");
		  $('[lang="en_US"] .uk-lightbox-content a.uk-slidenav-next').attr("title", "next image");
		  $('.uk-modal a.uk-modal-close').trigger("focus");
		},1000);
  });
  $('body').on("hide.uk.modal",'.uk-modal', function () {
	 console.log("Element is not visible.")
     $('.photo_list>a.uk-thumbnail>img').each(function () {
		if ($(this).attr('alt') == $('.uk-modal a.uk-modal-close~.uk-modal-caption').text()) {
			$(this).parent().trigger("focus")
		}
	 });
  });

  // Tab foucus
  $(".dropMenu .uk-button-dropdown>a, #sitemap>div:first-child a").on("focus", function () {
    $(this).trigger("click")
  })

  // Set role="button" to <a>
  $(".fontsize .small a, .fontsize .medium a, .fontsize .large a, #dropMenuR>li>a, a[title=網站全文檢索]").attr("role", "button")
  $("#btn_canvas a[data-uk-offcanvas], ul.uk-slideshow~a, ul.uk-slider a, #sitemap>div:first-child a, td.type>a").attr("role", "link")
  $(window).on('load', function () {
    $('#dropMenuR>li>a>svg').attr("role", "button")
    // console.log( "ready!" );
  }); // For AA of the right dropmenu icon

  $("footer .socialLinks span.fa-layers").removeAttr('title')

  //Tabindex of Slideshow
  $(".slideshow a").attr("tabindex", "1")

  //font resize
  $("input").addClass("font_resize")
  $('.large>a').on("click", function () {
    $('.font_resize, input').removeClass('font_medium').addClass('font_large')
  })
  $('.medium>a').on("click", function () {
    $('.font_resize, input').removeClass('font_large').addClass('font_medium')
  })
  $('.small>a').on("click", function () {
    $('.font_resize, input').removeClass('font_large').removeClass('font_medium')
  })

  //#dropMenuR
  $('#dropMenuR .uk-button-dropdown[data-uk-dropdown]:not(:last-child)').on('show.uk.dropdown', function () {
    console.log('open')
    $("#dropMenuR").addClass("borderRadiusLRT")
  })
  $('#dropMenuR .uk-button-dropdown[data-uk-dropdown]').on('hide.uk.dropdown', function () {
    $("#dropMenuR").removeClass("borderRadiusLRT")
  })
  $('#dropMenuR>li>a').on("focus", function () {
    $('#dropMenuR>li').not($(this).parent()).removeClass('uk-open')
  })

  //Remove parent if child empty
  $("p:empty, h1:empty, h2:empty, h3:empty, h4:empty, h5:empty, h6:empty").parent(".uk-overlay-panel").remove()
  //Remove if empty
  $("p:empty, h1:empty, h2:empty, h3:empty, h4:empty, h5:empty, h6:empty").remove()

  //
  $('.listTabs .uk-nav>li:not(.uk-active):not(.uk-parent)>a, .listTabs .uk-nav>li:not(.level3) li:not(.uk-active):not(.uk-parent)>a, .listTabs .uk-nav>li.level3 li li:not(.uk-active):not(.uk-parent)>a').on("click", function () {
    $(window).scrollTop(0)
    // $("html, body").animate({ scrollTop: 0 }, "slow");
  })

  //for tabs without switcher
  // $('.listTabs li:not(.uk-parent)>a').on("click", function() {
  //   $('.listTabs .uk-nav-sub li').removeClass('uk-active') //for 2 or more levels of tabs
  //   $(this).parent().addClass('uk-active').siblings().removeClass('uk-active');
  // });

})

//load active tab from url - CANNOT placed in $(document).ready
UIkit.on('beforeready.uk.dom', function () {
  var hash = document.location.hash
  if (hash) {
    UIkit.$(hash).addClass('uk-active')
    // UIkit.$(hash).addClass('uk-active').siblings().removeClass('uk-active').parent().parent("li").addClass('uk-active');
    //for 3 levels of tabs
  }
})